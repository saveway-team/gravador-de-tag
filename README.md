# Gravador de Tag

## To do versão 1.x
- Tornar possível fazer leitura enquanto faz gravação cíclica
- Tornar possível marcar/desmarcar o checkbox de Caixa S durante a gravação cíclica
- Botão parar: salvar as ids não gravadas (se houverem) em um arquivo separado para que na próxima vez que houver uma gravação (cíclica ou não) esses ids sejam usados
- Quando uma caixa s é gravada com o botão de gravação simples, sua string de id é gravada completa (10xxxxxxxxxx) no arquivo de saída, quando é gravada com gravação cíclica apenas a id em si é gravada no arquivo de saída
- Aba achar tag: aba onde é possível inserir um id (ou ids) e descobrir em que dia esse id foi gravado

## To do versão 2.x
- Eliminar o tagfile e gravação cíclica: descobrir como os ids são gerados para que seja possível gerá-los no momento da gravação, eliminando a necessidade do tagfile e da gravação cíclica
- Armazenar as ids já gravadas num bd (sqlite?) e na hora de gravar simplesmente dar um "select * from ids gravadas where id = [ID SENDO GRAVADA NO MOMENTO]"
- Gerar um arquivo ou relatório ou algo assim no final da gravação e fazer upload dele em alguma nuvem
