﻿namespace Gravador_de_Tag
{
    partial class formPrincipal
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formPrincipal));
            this.btnLer = new System.Windows.Forms.Button();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtPais = new System.Windows.Forms.TextBox();
            this.btnGravar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioTeclado = new System.Windows.Forms.RadioButton();
            this.radioBotao = new System.Windows.Forms.RadioButton();
            this.radioAutomatico = new System.Windows.Forms.RadioButton();
            this.btnCiclica = new System.Windows.Forms.Button();
            this.checkCaixaS = new System.Windows.Forms.CheckBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblProxTag = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuItemAcharTag = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.btnParar = new System.Windows.Forms.Button();
            this.btnProximo = new System.Windows.Forms.Button();
            this.listIDsGravadas = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.lblRestantesTagFile = new System.Windows.Forms.Label();
            this.lblRestantes = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLer
            // 
            this.btnLer.Location = new System.Drawing.Point(28, 211);
            this.btnLer.Name = "btnLer";
            this.btnLer.Size = new System.Drawing.Size(60, 23);
            this.btnLer.TabIndex = 4;
            this.btnLer.Text = "Ler";
            this.btnLer.UseVisualStyleBackColor = true;
            this.btnLer.Click += new System.EventHandler(this.btnLer_Click);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(58, 61);
            this.txtID.MaxLength = 10;
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(109, 20);
            this.txtID.TabIndex = 1;
            this.txtID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtID_KeyPress);
            // 
            // txtPais
            // 
            this.txtPais.Location = new System.Drawing.Point(58, 35);
            this.txtPais.MaxLength = 3;
            this.txtPais.Name = "txtPais";
            this.txtPais.Size = new System.Drawing.Size(109, 20);
            this.txtPais.TabIndex = 0;
            this.txtPais.Text = "76";
            this.txtPais.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPais_KeyPress);
            // 
            // btnGravar
            // 
            this.btnGravar.Location = new System.Drawing.Point(94, 211);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(60, 23);
            this.btnGravar.TabIndex = 5;
            this.btnGravar.Text = "Gravar";
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "País";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "ID";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioTeclado);
            this.groupBox1.Controls.Add(this.radioBotao);
            this.groupBox1.Controls.Add(this.radioAutomatico);
            this.groupBox1.Location = new System.Drawing.Point(15, 110);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(152, 91);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Controle de gravação";
            // 
            // radioTeclado
            // 
            this.radioTeclado.AutoSize = true;
            this.radioTeclado.Location = new System.Drawing.Point(13, 42);
            this.radioTeclado.Name = "radioTeclado";
            this.radioTeclado.Size = new System.Drawing.Size(64, 17);
            this.radioTeclado.TabIndex = 1;
            this.radioTeclado.Text = "Teclado";
            this.radioTeclado.UseVisualStyleBackColor = true;
            // 
            // radioBotao
            // 
            this.radioBotao.AutoSize = true;
            this.radioBotao.Checked = true;
            this.radioBotao.Location = new System.Drawing.Point(13, 19);
            this.radioBotao.Name = "radioBotao";
            this.radioBotao.Size = new System.Drawing.Size(53, 17);
            this.radioBotao.TabIndex = 0;
            this.radioBotao.TabStop = true;
            this.radioBotao.Text = "Botão";
            this.radioBotao.UseVisualStyleBackColor = true;
            // 
            // radioAutomatico
            // 
            this.radioAutomatico.AutoSize = true;
            this.radioAutomatico.Location = new System.Drawing.Point(13, 65);
            this.radioAutomatico.Name = "radioAutomatico";
            this.radioAutomatico.Size = new System.Drawing.Size(78, 17);
            this.radioAutomatico.TabIndex = 2;
            this.radioAutomatico.Text = "Automático";
            this.radioAutomatico.UseVisualStyleBackColor = true;
            // 
            // btnCiclica
            // 
            this.btnCiclica.Location = new System.Drawing.Point(28, 240);
            this.btnCiclica.Name = "btnCiclica";
            this.btnCiclica.Size = new System.Drawing.Size(126, 23);
            this.btnCiclica.TabIndex = 6;
            this.btnCiclica.Text = "Gravação Cíclica";
            this.btnCiclica.UseVisualStyleBackColor = true;
            this.btnCiclica.Click += new System.EventHandler(this.btnCiclica_Click);
            // 
            // checkCaixaS
            // 
            this.checkCaixaS.AutoSize = true;
            this.checkCaixaS.Location = new System.Drawing.Point(15, 87);
            this.checkCaixaS.Name = "checkCaixaS";
            this.checkCaixaS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkCaixaS.Size = new System.Drawing.Size(62, 17);
            this.checkCaixaS.TabIndex = 2;
            this.checkCaixaS.Text = "Caixa S";
            this.checkCaixaS.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.lblProxTag});
            this.statusStrip1.Location = new System.Drawing.Point(0, 275);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(421, 22);
            this.statusStrip1.TabIndex = 15;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(73, 17);
            this.lblStatus.Text = "Aguardando";
            // 
            // lblProxTag
            // 
            this.lblProxTag.Name = "lblProxTag";
            this.lblProxTag.Size = new System.Drawing.Size(162, 17);
            this.lblProxTag.Text = "|  Gravando próxima tag em 5";
            this.lblProxTag.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemAcharTag,
            this.menuItemConfig});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(421, 24);
            this.menuStrip1.TabIndex = 16;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuItemAcharTag
            // 
            this.menuItemAcharTag.Name = "menuItemAcharTag";
            this.menuItemAcharTag.Size = new System.Drawing.Size(72, 20);
            this.menuItemAcharTag.Text = "Achar Tag";
            this.menuItemAcharTag.Click += new System.EventHandler(this.menuItemAcharTag_Click);
            // 
            // menuItemConfig
            // 
            this.menuItemConfig.Name = "menuItemConfig";
            this.menuItemConfig.Size = new System.Drawing.Size(96, 20);
            this.menuItemConfig.Text = "Configurações";
            this.menuItemConfig.Click += new System.EventHandler(this.menuItemConfig_Click);
            // 
            // btnParar
            // 
            this.btnParar.Enabled = false;
            this.btnParar.Location = new System.Drawing.Point(95, 240);
            this.btnParar.Name = "btnParar";
            this.btnParar.Size = new System.Drawing.Size(59, 23);
            this.btnParar.TabIndex = 99;
            this.btnParar.Text = "Parar";
            this.btnParar.UseVisualStyleBackColor = true;
            this.btnParar.Visible = false;
            this.btnParar.Click += new System.EventHandler(this.btnParar_Click);
            // 
            // btnProximo
            // 
            this.btnProximo.Enabled = false;
            this.btnProximo.Location = new System.Drawing.Point(28, 240);
            this.btnProximo.Name = "btnProximo";
            this.btnProximo.Size = new System.Drawing.Size(60, 23);
            this.btnProximo.TabIndex = 100;
            this.btnProximo.Text = "Próximo";
            this.btnProximo.UseVisualStyleBackColor = true;
            this.btnProximo.Visible = false;
            this.btnProximo.Click += new System.EventHandler(this.btnProximo_Click);
            // 
            // listIDsGravadas
            // 
            this.listIDsGravadas.FormattingEnabled = true;
            this.listIDsGravadas.Location = new System.Drawing.Point(190, 64);
            this.listIDsGravadas.Name = "listIDsGravadas";
            this.listIDsGravadas.Size = new System.Drawing.Size(212, 199);
            this.listIDsGravadas.TabIndex = 101;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(187, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 102;
            this.label4.Text = "Gravadas:";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(246, 38);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(13, 13);
            this.lblCount.TabIndex = 103;
            this.lblCount.Text = "0";
            // 
            // lblRestantesTagFile
            // 
            this.lblRestantesTagFile.AutoSize = true;
            this.lblRestantesTagFile.Location = new System.Drawing.Point(265, 38);
            this.lblRestantesTagFile.Name = "lblRestantesTagFile";
            this.lblRestantesTagFile.Size = new System.Drawing.Size(111, 13);
            this.lblRestantesTagFile.TabIndex = 104;
            this.lblRestantesTagFile.Text = "Restantes no TagFile:";
            this.lblRestantesTagFile.Visible = false;
            // 
            // lblRestantes
            // 
            this.lblRestantes.AutoSize = true;
            this.lblRestantes.Location = new System.Drawing.Point(379, 38);
            this.lblRestantes.Name = "lblRestantes";
            this.lblRestantes.Size = new System.Drawing.Size(13, 13);
            this.lblRestantes.TabIndex = 105;
            this.lblRestantes.Text = "0";
            this.lblRestantes.Visible = false;
            // 
            // formPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 297);
            this.Controls.Add(this.lblRestantes);
            this.Controls.Add(this.lblRestantesTagFile);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listIDsGravadas);
            this.Controls.Add(this.btnProximo);
            this.Controls.Add(this.btnParar);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.checkCaixaS);
            this.Controls.Add(this.btnCiclica);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGravar);
            this.Controls.Add(this.txtPais);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.btnLer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "formPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SaveTag";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.formPrincipal_KeyUp);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLer;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.TextBox txtPais;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioTeclado;
        private System.Windows.Forms.RadioButton radioBotao;
        private System.Windows.Forms.RadioButton radioAutomatico;
        private System.Windows.Forms.Button btnCiclica;
        private System.Windows.Forms.CheckBox checkCaixaS;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuItemConfig;
        private System.Windows.Forms.ToolStripMenuItem menuItemAcharTag;
        private System.Windows.Forms.Button btnParar;
        private System.Windows.Forms.Button btnProximo;
        private System.Windows.Forms.ListBox listIDsGravadas;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.ToolStripStatusLabel lblProxTag;
        private System.Windows.Forms.Label lblRestantesTagFile;
        private System.Windows.Forms.Label lblRestantes;
    }
}

