﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gravador_de_Tag {
    public partial class formConfig : Form {
        public formConfig() {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);

            if ((int) Properties.Settings.Default["Lock"] == 1)
                checkBoxLock.Checked = true;
            else
                checkBoxLock.Checked = false;

            txtCaminho.Text = (string) Properties.Settings.Default["CaminhoTagFile"];

            btnTeclaProx.Text = ((Keys) Properties.Settings.Default["TeclaProxTag"]).ToString();

            segundosSleep.Value = (int) Properties.Settings.Default["SleepNovaTag"];
        }

        protected override void OnClosed(EventArgs e) {
            base.OnClosed(e);

            if (checkBoxLock.Checked == true)
                Properties.Settings.Default["Lock"] = 1;
            else
                Properties.Settings.Default["Lock"] = 0;

            Properties.Settings.Default["CaminhoTagFile"] = txtCaminho.Text;

            Properties.Settings.Default["SleepNovaTag"] = (int) segundosSleep.Value;

            Properties.Settings.Default.Save();

        }

        private void btnProcurar_Click(object sender, EventArgs e) {
            openFileDialog1.Multiselect = false;
            openFileDialog1.Title = "Selecione o TagFile";
            openFileDialog1.Filter = "Arquivos texto (*.txt)|*.txt";
            openFileDialog1.FileName = "";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            DialogResult resultado = openFileDialog1.ShowDialog();
            if (resultado == System.Windows.Forms.DialogResult.OK)
                txtCaminho.Text = openFileDialog1.FileName;
        }

        private void btnTeclaProx_Click(object sender, EventArgs e) {
            FormKeyBind keyBind = new FormKeyBind();
            keyBind.ShowDialog(this);
        }

        public void SetTextBtnTeclaProx(string text) {
            btnTeclaProx.Text = text;
        }
    }
}
