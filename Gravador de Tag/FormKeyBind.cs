﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gravador_de_Tag {
    public partial class FormKeyBind : Form {
        public FormKeyBind() {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);

            label1.Text = ((Keys)Properties.Settings.Default["TeclaProxTag"]).ToString();
        }

        private void FormKeyBind_KeyUp(object sender, KeyEventArgs e) {
            label1.Text = e.KeyCode.ToString();
            Properties.Settings.Default["TeclaProxTag"] = (int) e.KeyCode;
        }

        protected override void OnClosed(EventArgs e) {
            base.OnClosed(e);

            Properties.Settings.Default.Save();

            formConfig config = (formConfig) this.Owner;
            config.SetTextBtnTeclaProx(label1.Text);
        }
    }
}
