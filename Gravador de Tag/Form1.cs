﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;

using rfid;

namespace Gravador_de_Tag {
    public partial class formPrincipal : Form {
        #region VARIÁVEIS
        private const int TTYP = GIS_LF_API.TTYP_HITAG_Y;
        private int portHandle;

        private List<UInt64> ids;
        private List<IDGravada> gravadas;

        private bool gravacaoCiclica = false;
        private int count = 0;
        #endregion

        #region MÉTODOS RELATIVOS AO LIFECYCLE DO FORM
        public formPrincipal() {
            InitializeComponent();
            conectar();

            gravadas = new List<IDGravada>();
            List<string> jaGravadas = File.ReadAllLines("saida.txt").ToList();
            gravadas = jaGravadas.ConvertAll(new Converter<string, IDGravada>(stringToIDGravada));
        }

        private void Form1_Load(object sender, EventArgs e) {}

        protected override void OnShown(EventArgs e) {
            base.OnShown(e);

            txtID.Focus();

            if (portHandle < 0) {
                MessageBox.Show("Não foi encontrado nenhum gravador! Certifique-se de que existe um dispositivo conectado ao computador.");
                lblStatus.Text = "Não foi detectado nenhum dispositivo de gravação!";
            }
        }

        protected override void OnClosed(EventArgs e) {
            base.OnClosed(e);

            if (portHandle > 0)
                GIS_LF_API.TSLF_Close(portHandle);

            File.Delete("saida.txt");
            File.Create("saida.txt").Close();
            StreamWriter writer = new StreamWriter("saida.txt");
            for (int i = 0; i < gravadas.Count; i++) {
                writer.Write(gravadas.ElementAt(i).toString() + Environment.NewLine);
                writer.Flush();
            }
            writer.Close();
        }
        #endregion

        #region MÉTODOS RELATIVOS AOS EVENTOS DOS COMPONENTES VISUAIS DO FORM
        private void btnLer_Click(object sender, EventArgs e) {
            if (portHandle > 0) {
                int country = 0, animalFlag = 0, blockFlag = 0, reserved = 0, extension = 0;
                Byte[] buffer = new Byte[8];

                if (GIS_LF_API.TSLF_Read_FDXB(portHandle, ref country, buffer, 8, ref animalFlag, ref blockFlag, ref reserved, ref extension) >= 0) {
                    txtPais.Text = country.ToString();
                    txtID.Text = bufferToUInt64(buffer).ToString();
                    lblStatus.Text = "Leitura bem sucedida!";
                }
                else {
                    int erro = GIS_LF_API.TSLF_GetLastError(portHandle);
                    switch (erro) {
                        case 21:
                            lblStatus.Text = "Erro - Código " + erro + " | No tag";
                            break;
                        default:
                            lblStatus.Text = "Erro - Código " + erro + " | Erro inesperado";
                            break;
                    }
                }
            }
            else
                MessageBox.Show("Não há nenhum dispositivo conectado no momento!");
        }

        private void btnGravar_Click(object sender, EventArgs e) {
            if (portHandle > 0) {
                if (isNumber(txtID.Text) && isNumber(txtPais.Text)) {
                    UInt64 atual;
                    if (checkCaixaS.Checked == true) {
                        string aux = txtID.Text;
                        while (aux.Length < 10)
                            aux = "0" + aux;
                        atual = Convert.ToUInt64("10" + aux);
                    }
                    else
                        atual = Convert.ToUInt64(txtID.Text);
                    List<IDGravada> jaGravada = new List<IDGravada>(0);
                    if (!idJaGravada(jaGravada, atual)) {
                        if (gravar(Convert.ToInt32(txtPais.Text), uint64ToBuffer(atual)) < 0) {
                            int erro = GIS_LF_API.TSLF_GetLastError(portHandle);
                            switch (erro) {
                                case 20:
                                    lblStatus.Text = "Erro - Código " + erro + " | Tag lock";
                                    MessageBox.Show("Essa tag provavelmente já foi gravada e está lockada!", "Erro - Código " + erro);
                                    break;
                                case 21:
                                    lblStatus.Text = "Erro - Código " + erro + " | No tag";
                                    MessageBox.Show("O dispositivo de gravação não foi capaz de comunicar-se com a tag!", "Erro - Código " + erro + " - No tag!");
                                    break;
                                default:
                                    lblStatus.Text = "Erro - Código " + erro + " | Erro inesperado";
                                    MessageBox.Show("Ocorreu um erro inesperado de código " + erro + ". Consulte o manual da SDK do gravador.", "Erro - Código " + erro);
                                    break;
                            }
                        }
                        else {
                            listIDsGravadas.Items.Insert(0, txtID.Text);
                            gravadas.Insert(0, new IDGravada(atual, (int)(DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0))).TotalSeconds));
                            lblStatus.Text = "Gravação bem sucedida!";
                            lblCount.Text = ++count + "";
                            Console.Beep();
                        }
                    }
                    else {
                        DateTime data = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        MessageBox.Show("Esta ID foi gravada no dia " + data.AddSeconds(jaGravada.First().Timestamp).ToShortDateString());
                    }
                }
                else
                    MessageBox.Show("Os campos precisam ser preenchidos com valores numéricos! Tente novamente.");
            }
            else
                MessageBox.Show("Não há nenhum dispositivo conectado no momento!");
        }

        private void btnCiclica_Click(object sender, EventArgs e) {
            if (!isNumber(txtPais.Text)) {
                MessageBox.Show("Preencha o campo País com um valor válido!");
                txtPais.Focus();
            }
            else {
                try {
                    List<string> tagFile = File.ReadAllLines((string)Properties.Settings.Default["CaminhoTagFile"]).ToList();
                    ids = tagFile.ConvertAll(new Converter<string, UInt64>(stringToUInt64));
                    List<IDGravada> jaGravadas = new List<IDGravada>(0);

                    if (!idJaGravada(jaGravadas, ids.ToArray())) {
                        if ((checkCaixaS.Checked == true &&
                        MessageBox.Show("O check box de Caixas S está marcado.\nVocê confirma que está gravando Caixas S?", "Gravando Caixas S?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        || (checkCaixaS.Checked == false &&
                        MessageBox.Show("O check box de Caixas S está desmarcado.\nVocê confirma que está gravando manchões?", "Gravando Caixas S?", MessageBoxButtons.YesNo) == DialogResult.Yes)) {
                            txtID.Text = ids.First() + "";

                            gravacaoCiclica = true;

                            txtPais.Enabled = false;
                            txtID.Enabled = false;
                            checkCaixaS.Enabled = false;
                            radioBotao.Enabled = false;
                            radioTeclado.Enabled = false;
                            radioAutomatico.Enabled = false;
                            btnLer.Enabled = false;
                            btnGravar.Enabled = false;
                            btnCiclica.Enabled = false;
                            btnCiclica.Visible = false;
                            menuItemAcharTag.Enabled = false;
                            menuItemConfig.Enabled = false;

                            lblRestantesTagFile.Visible = true;
                            lblRestantes.Visible = true;
                            lblRestantes.Text = ids.Count + "";

                            if (radioBotao.Checked == true) {
                                btnProximo.Enabled = true;
                                btnProximo.Visible = true;
                                btnParar.Enabled = true;
                                btnParar.Visible = true;
                            }
                            else {
                                btnParar.Left = btnCiclica.Left;
                                btnParar.Width = btnCiclica.Width;
                                btnParar.Enabled = true;
                                btnParar.Visible = true;
                                //if (radioAutomatico.Checked == true) {
                                //    lblProxTag.Visible = true;
                                //    gravaAutomatico();
                                //}
                            }
                        }
                    }
                    else {
                        string msg = "";
                        foreach (IDGravada gravada in jaGravadas) {
                            DateTime data = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                            msg += "A ID " + gravada.Id + " foi gravada no dia " + data.AddSeconds(gravada.Timestamp).ToShortDateString() + Environment.NewLine;
                        }
                        MessageBox.Show(msg);

                        ////////////////////// TODO COMO PROCEDER SE TAGFILE TIVER TAG JA GRAVADA
                        /// PERGUNTAR SE DESEJA PARAR O PROCESSO E LIDAR COM ISSO SOZINHO
                        /// PERGUNTAR SE DESEJA SIMPLESMENTE EXCLUIR ESSAS IDS DO TAGFILE
                        /// PERGUNTAR SE DESEJA GERAR PENDÊNCIA
                    }
                } catch (FileNotFoundException ex) {
                    MessageBox.Show("Não foi posssível encontrar o TagFile. Especifique o seu caminho na aba de configurações!");
                }
            }
        }

        private void formPrincipal_KeyUp(object sender, KeyEventArgs e) {
            if (gravacaoCiclica && radioTeclado.Checked == true && e.KeyCode == (Keys) Properties.Settings.Default["TeclaProxTag"])
                gravaProximo();
            e.Handled = true;
        }

        private void btnProximo_Click(object sender, EventArgs e) {
            gravaProximo();
        }

        private void btnParar_Click(object sender, EventArgs e) {
            paraGravacao();
        }

        private void menuItemConfig_Click(object sender, EventArgs e) {
            formConfig config = new formConfig();
            config.ShowDialog();
        }

        private void menuItemAcharTag_Click(object sender, EventArgs e) {
            MessageBox.Show("WIP");
        }

        private void txtID_KeyPress(object sender, KeyPressEventArgs e) {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtPais_KeyPress(object sender, KeyPressEventArgs e) {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        #endregion

        #region MÉTODOS REFERENTES AO FLUXO DE GRAVAÇÃO DAS TAGS
        private void gravaProximo() {
            UInt64 atual;
            if (checkCaixaS.Checked == true) {
                string aux = txtID.Text;
                while (aux.Length < 10)
                    aux = "0" + aux;
                atual = Convert.ToUInt64("10" + aux);
            }
            else
                atual = Convert.ToUInt64(txtID.Text);
            if (gravar(Convert.ToInt32(txtPais.Text), uint64ToBuffer(atual)) < 0) {
                int erro = GIS_LF_API.TSLF_GetLastError(portHandle);
                switch (erro) {
                    case 20:
                        lblStatus.Text = "Erro - Código " + erro + " | Tag lock";
                        MessageBox.Show("Essa tag provavelmente já foi gravada e está lockada!", "Erro - Código " + erro);
                        break;
                    case 21:
                        lblStatus.Text = "Erro - Código " + erro + " | No tag";
                        MessageBox.Show("O dispositivo de gravação não foi capaz de comunicar-se com a tag!", "Erro - Código " + erro + " - No tag!");
                        break;
                    default:
                        lblStatus.Text = "Erro - Código " + erro + " | Erro inesperado";
                        MessageBox.Show("Ocorreu um erro inesperado de código " + erro + ". Consulte o manual da SDK do gravador.", "Erro - Código " + erro);
                        break;
                }
            }
            else {
                Console.Beep();
                lblStatus.Text = "Gravação bem sucedida!";
                lblCount.Text = ++count + "";
                listIDsGravadas.Items.Insert(0, ids.ElementAt(0));
                gravadas.Insert(0, new IDGravada(Convert.ToUInt64(txtID.Text), (int)(DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0))).TotalSeconds));

                if (ids.Count > 1) {
                    ids.RemoveAt(0);
                    txtID.Text = ids.ElementAt(0) + "";
                    lblRestantes.Text = ids.Count + "";
                }
                else
                    paraGravacao();
            }
        }

        private void paraGravacao() {
            gravacaoCiclica = false;

            if (ids.Count > 1)
                MessageBox.Show("Ainda existem tags para gravar");
            else {
                lblRestantes.Text = "0";
                txtPais.Enabled = true;
                txtID.Enabled = true;
                txtID.Text = "";
                checkCaixaS.Enabled = true;
                radioBotao.Enabled = true;
                radioTeclado.Enabled = true;
                radioAutomatico.Enabled = true;
                btnLer.Enabled = true;
                btnGravar.Enabled = true;
                btnCiclica.Enabled = true;
                btnCiclica.Visible = true;
                menuItemAcharTag.Enabled = true;
                menuItemConfig.Enabled = true;

                btnProximo.Enabled = false;
                btnProximo.Visible = false;
                btnParar.Enabled = false;
                btnParar.Visible = false;
            }
        }

        private int gravar(Int32 pais, byte[] id) {
            int lockar = (int) Properties.Settings.Default["Lock"];

            Byte[] factoryID = new Byte[8];
            factoryID[0] = 0;
            factoryID[1] = 0;
            factoryID[2] = 0;
            factoryID[3] = 0;
            factoryID[4] = 0;
            factoryID[5] = 0;
            factoryID[6] = 0;
            factoryID[7] = 0;

            int retorno = GIS_LF_API.TSLF_Write_FDXB(portHandle, TTYP, factoryID, pais, id, 8, 0, 0, 0, 0, lockar);
            GIS_LF_API.TSLF_ResetTransponder(portHandle, 0);
            return retorno;
        }
        #endregion

        #region MÉTODOS DE UTILIDADES (COMO CONVERSÕES DE TIPOS, VERIFICAÇÕES BOOLEANAS, ETC)
        private UInt64 bufferToUInt64(byte[] b) {
            UInt64 result = 0;

            for (int i = b.Length - 1; i >= 0; i--) {
                result <<= 8;
                result += b[i];
            }

            return result;
        }

        private byte[] uint64ToBuffer(UInt64 num) {
            byte[] buffer = new byte[8];

            for (int i = 0; i < 8; i++) {
                buffer[i] = (byte)(num & 0x00FF);
                num >>= 8;
            }

            return buffer;
        }

        private bool isNumber(string value) {
            if (value.Length == 0)
                return false;

            for (int i = 0; i < value.Length; i++)
                if (!Char.IsDigit(value[i]))
                    return false;

            return true;
        }

        private bool idJaGravada(List<IDGravada> list, params UInt64[] id) {
            foreach (UInt64 aux in id) {
                int i = gravadas.FindIndex(x => x.Id == aux);
                if (i > -1)
                    list.Add(gravadas.ElementAt(i));
            }

            return list.Count > 0;
        }

        private static UInt64 stringToUInt64(string s) {
            return Convert.ToUInt64(s);
        }

        private static IDGravada stringToIDGravada(string s) {
            string[] aux = s.Split('_');
            return new IDGravada(Convert.ToUInt64(aux[0]), Convert.ToInt32(aux[1]));
        }
        #endregion

        private void conectar() {
            int maxLength = 255;
            SByte[] device = new SByte[maxLength];
            GIS_LF_API.TSLF_GetUSBDeviceNames(device, maxLength);
            portHandle = GIS_LF_API.TSLF_Open(device, 19200, 0, 2000);
            //portHandle = 10;
        }
    }
}