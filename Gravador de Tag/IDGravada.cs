﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravador_de_Tag {
    class IDGravada {
        public UInt64 Id { get; set; }
        public int Timestamp{ get; set; }

        public IDGravada(UInt64 id, int timestamp) {
            Id = id;
            Timestamp = timestamp;
        }

        public string toString() {
            return Id + "_" + Timestamp;
        }
    }
}
